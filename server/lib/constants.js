module.exports = {
  STATUS_CODE_SUCCESS: 200,
  STATUS_CODE_CREATE_SUCCESS: 201,
  STATUS_CODE_ACCEPTED: 202,
  STATUS_CODE_BAD_REQUEST: 400,
  STATUS_CODE_RESOURCE_FORBIDDEN: 403,
  STATUS_CODE_RESOURCE_NOT_FOUND: 404,
  STATUS_CODE_BAD_MEDIA_TYPE: 415,
  STATUS_CODE_SERVER_ERROR: 500,
  STATUS_CODE_DOWNSTREAM_ERROR: 503,
  STATUS_CODE_SERVER_TIMEOUT: 408,

  SORT_ASC: "asc",

  ADMIN_VIEW: "OCD Admin View all entries",

  JSON_TYPE: "json",

  DONOR_NAME: "Name of Donor/Contact Person",
};
