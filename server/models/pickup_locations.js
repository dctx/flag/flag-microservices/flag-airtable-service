/* jshint indent: 2 */
"use strict";

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "PickupLocations",
    {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      donor_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "donors",
          key: "id",
        },
      },
      address_line_one: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      barangay: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      city: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      landmark: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      province: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      tableName: "pickup_locations",
    }
  );
};
