/* jshint indent: 2 */
"use strict";

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "OtpRequests",
    {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      mobile_number: {
        type: "ARRAY",
        allowNull: false,
        unique: true,
      },
      transaction_type: {
        type: "ARRAY",
        allowNull: false,
      },
      reference_code: {
        type: "ARRAY",
        allowNull: false,
      },
      validation_code: {
        type: "ARRAY",
        allowNull: false,
      },
      date: {
        type: DataTypes.DATEONLY,
        allowNull: false,
      },
    },
    {
      tableName: "otp_requests",
    }
  );
};
