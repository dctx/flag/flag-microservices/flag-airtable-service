/* jshint indent: 2 */
"use strict";

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "Fleets",
    {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      fleet_leader: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      fleet_contact: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {
      tableName: "fleets",
    }
  );
};
