/* jshint indent: 2 */
"use strict";

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "Packages",
    {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      donor_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "donors",
          key: "id",
        },
      },
      package_remarks: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      photo_reference: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {
      tableName: "packages",
    }
  );
};
