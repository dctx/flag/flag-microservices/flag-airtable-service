/* jshint indent: 2 */
"use strict";

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "OutboundDispatches",
    {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      destination_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "destinations",
          key: "id",
        },
      },
      package_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "packages",
          key: "id",
        },
      },
      donation_status_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "donation_statuses",
          key: "id",
        },
      },
      fleet_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "fleets",
          key: "id",
        },
      },
      dispatch_status_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "dispatch_statuses",
          key: "id",
        },
      },
      unit_of_measure: {
        type: DataTypes.DOUBLE,
        allowNull: true,
      },
      unit_value: {
        type: DataTypes.DOUBLE,
        allowNull: true,
      },
      completion_remark: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      delivery_timestamp: {
        type: DataTypes.TIME,
        allowNull: true,
      },
    },
    {
      tableName: "outbound_dispatches",
    }
  );
};
