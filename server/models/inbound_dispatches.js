/* jshint indent: 2 */
"use strict";

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "InboundDispatches",
    {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      pickup_location_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "pickup_locations",
          key: "id",
        },
      },
      package_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "packages",
          key: "id",
        },
      },
      donation_status_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "donation_statuses",
          key: "id",
        },
      },
      fleet_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "fleets",
          key: "id",
        },
      },
      dispatch_status_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "dispatch_statuses",
          key: "id",
        },
      },
      request_remark: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      pickup_timestamp: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      tableName: "inbound_dispatches",
    }
  );
};
