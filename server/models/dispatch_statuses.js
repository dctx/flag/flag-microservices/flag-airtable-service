/* jshint indent: 2 */
"use strict";

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "DispatchStatuses",
    {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {
      tableName: "dispatch_statuses",
    }
  );
};
