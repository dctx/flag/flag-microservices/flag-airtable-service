/* jshint indent: 2 */
"use strict";

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "PackageContents",
    {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      item_category_id: {
        type: DataTypes.BIGINT,
        allowNull: true,
        references: {
          model: "item_categories",
          key: "id",
        },
      },
      weight: {
        type: DataTypes.DOUBLE,
        allowNull: true,
      },
      monetary_value: {
        type: DataTypes.DOUBLE,
        allowNull: true,
      },
      unit_of_measure: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      total_unit_value: {
        type: DataTypes.DOUBLE,
        allowNull: true,
      },
      remaining_unit_value: {
        type: DataTypes.DOUBLE,
        allowNull: true,
      },
      category_remarks: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {
      tableName: "package_contents",
    }
  );
};
