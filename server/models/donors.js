/* jshint indent: 2 */
"use strict";

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "Donors",
    {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      affiliation_org: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      representative_name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      mobile_number: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      donor_type_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "donor_types",
          key: "id",
        },
      },
      email: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      sector_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: "sectors",
          key: "id",
        },
      },
    },
    {
      tableName: "donors",
    }
  );
};
