const airtable = require("./airtable");
const donor = require("./donor");

module.exports = {
  donor,
  airtable,
};
