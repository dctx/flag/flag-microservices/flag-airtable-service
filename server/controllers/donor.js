const Donor = require("../models").Donor;

module.exports = {
  findAll(req, res) {
    return Donor.findAll()
      .then((donors) => res.status(200).send(donors))
      .catch((error) => res.status(400).send(error));
  },
};
