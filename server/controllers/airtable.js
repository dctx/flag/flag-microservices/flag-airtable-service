const Airtable = require("../lib/airtable");
let {
  STATUS_CODE_SUCCESS,
  STATUS_CODE_BAD_REQUEST,
  SORT_ASC,
  ADMIN_VIEW,
  DONOR_NAME,
  JSON_TYPE,
} = require("../lib/constants");

module.exports = {
  findAll(req, res) {
    const airtable = new Airtable({ apiKey: process.env.AIRTABLE_API_KEY })
      .base(process.env.AIRTABLE_BASE_KEY)
      .table(process.env.AIRTABLE_TABLE);

    var query = {
      sort: [{ field: DONOR_NAME, direction: SORT_ASC }],
      view: ADMIN_VIEW,
      cellFormat: JSON_TYPE,
    };

    let filterByFormula = req.query.filterByFormula;
    if (filterByFormula) {
      query["filterByFormula"] = filterByFormula;
    }

    return airtable
      .list(query)
      .then((resp) => {
        res.status(STATUS_CODE_SUCCESS).send(resp);
      })
      .catch((error) => {
        res.status(STATUS_CODE_BAD_REQUEST).send(error);
      });
  },
};
