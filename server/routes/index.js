const donorController = require("../controllers").donor;
const airtableController = require("../controllers").airtable;

module.exports = (app) => {
  app.get("/airtable", airtableController.findAll);
  app.get("/donor", donorController.findAll);
};
